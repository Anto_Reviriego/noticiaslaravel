<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoticiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('noticias', function (Blueprint $table) {
            $table->id();
            $table->string('titulo', 250);
            $table->text('cuerpo');
            $table->string('imagen', 250)->nullable(); 
            $table->string('archivo1', 250)->nullable(); 
            $table->string('archivo2', 250)->nullable(); 
            $table->string('archivo3', 250)->nullable(); 
            $table->bigInteger('autor')->unsigned();
            $table->foreign('autor')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('noticias');
    }
}
