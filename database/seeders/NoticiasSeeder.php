<?php

namespace Database\Seeders;

use App\Models\Noticia;
use Illuminate\Database\Seeder;

class NoticiasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // noticias desde la api lorem ipsum
        /*for ($i=1; $i <= 10; $i++) { 
            $noti = new Noticia(); 
            $noti->titulo = "Noticia $i"; 
            $noti->cuerpo = file_get_contents('https://baconipsum.com/api/?type=all-meat&paras=3&start-with-lorem=1&format=html'); // obtengo parrafos
            $noti->save();
        }*/
        Noticia::factory()->count(10)->create();
        /* deleted_at */
        // $noti = Noticia::find(1);
        // $noti->delete();

    }
}
