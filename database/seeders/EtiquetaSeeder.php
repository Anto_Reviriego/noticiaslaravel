<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Noticia;
use App\Models\Etiqueta;
use Illuminate\Database\Seeder;

class EtiquetaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Etiqueta::factory()->count(40)->create();
        Noticia::chunk(2, function($noticias){
            foreach ($noticias as $key => $noticia) {
               $eRandom = Etiqueta::all()->random(rand(1,5)); 
               $user = User::all()->random()->id;
               $noticia->etiquetaAutorNoticia()->attach($eRandom, ['user_id' => $user]);
            }
        });
        
    }
}
