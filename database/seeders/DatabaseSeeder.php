<?php

namespace Database\Seeders;

use App\Models\Examen;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

       // $this->call();
        $this->call([
            RolesSeeder::class,
            UserSeeder::class,
            NoticiasSeeder::class,
            EtiquetaSeeder::class,
            CarreraSeeder::class,
            CategoriaSeeder::class, 
            MateriaSeeder::class, 
            ExamenSeeder::class,
            
        ]);

        // Noticia::factory(40)->create();
        // Etiqueta::factory(40)->create();
    }
}
