<?php

namespace Database\Factories;

use App\Models\Noticia;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class NoticiaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Noticia::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $created = $this->faker->dateTimeBetween('-5 years');
        return [
            'titulo' => $this->faker->sentence(5),
            'cuerpo' => $this->faker->paragraph(35),
            'imagen' => $this->faker->optional()->imageUrl(400,200),
            'created_at' => $created, 
            'updated_at' => $this->faker->dateTimeBetween($created),
            'autor' => User::all()->random()->id,
        ];
    }
}
