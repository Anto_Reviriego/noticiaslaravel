<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Carrera;
use App\Models\Noticia;
use App\Models\Etiqueta;
use App\Models\Categoria;
use App\Notifications\NoticiaLeer;
use App\Notifications\NoticiaNueva;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class NoticiaController extends Controller
{
    public function index($page = 6){
        $noticias = Noticia::paginate($page);
        return view('Noticia.index', compact('noticias'));
    }

    /* FILTROS Y PAGINADOS */

    public function cardNoticiaPorAutor($a, $page = 6){
        $autor = User::find($a);
        $noticias = $autor->noticiasUser()->with('autorNoticia')->with('etiquetaAutorNoticia')->paginate($page);
        return view('Noticia.index', compact('noticias'));
    }

    public function cardNoticiaPorCarrera($c, $page = 6){
        $carrera = Carrera::find($c);
        $noticias = $carrera->noticiasCarrera()->paginate($page);
        return view('Noticia.index', compact('noticias'));
    }

    public function cardNoticiaPorEtiqueta($e, $page = 6){
        $etiqueta = Etiqueta::findOrFail($e);
        $eNoticias = $etiqueta->noticiasEtiqueta()->paginate($page);
        return view('Noticia.index', [ 'noticias' => $eNoticias]);
    }

    /************************************************************************/
    
    public function create(){
        $users = User::pluck('name', 'id');
        $Login = Auth::User()->id; //
        $etiquetas = Etiqueta::pluck('nombre', 'id');
        $carreras = Carrera::pluck('carrera', 'id');
        $categorias = Categoria::pluck('categoria', 'id');
        return view('Noticia.create', compact('users', 'Login', 'etiquetas', 'carreras', 'categorias'));
    }

    public function store(Request $request){
        $etiquetas = Etiqueta::all(); 
        $user =  Auth::User()->id;
        // VALIDEZ DE DATOS
        $data = $request->validate([
            'titulo' => 'required|unique:noticias',
            'cuerpo' => 'required',
            'carrera_id' => 'required',
            // 'categoria_id' => 'required',
            'imagen' => 'image|mimes:JPG,JPEG,png,IMG|max:2048',
            'archivo1' => 'file|max:2048',
            'archivo2' => 'file|max:2048',
            'archivo3' => 'file|max:2048'
        ]);
        
        // INSTANCIA NOTICIA
       
        $n = new Noticia();
        $n->titulo = $request->input('titulo');
        $n->cuerpo = $request->input('cuerpo');
        $n->imagen = $request->file('imagen');
        $n->archivo1 = $request->file('archivo1');
        $n->archivo2 = $request->file('archivo2');
        $n->archivo3 = $request->file('archivo3');
        $n->autor = $user;
        $n->carrera_id = $request->input('carrera_id');
        // $n->categoria_id = $request->input('categoria_id');
        $n->save();
       //dd( $n);
        if($request->hasFile('imagen')){
            $fileImg = $request->file('imagen');
            $path = $fileImg->storeAs('public/noticias/' . $n->id, $fileImg->getClientOriginalName()); 
            $savePath = str_replace('public/', '', $path);
            $n->imagen = $savePath;
            $n->save();
        }

        for ($i=1; $i <=3; $i++) { 
            if($request->hasFile('archivo'.$i)){
                $file = $request->file('archivo'.$i);
                $path = $file->storeAs('public/archivos/' . $n->id, $file->getClientOriginalName()); 
                $savePath = str_replace('public/', '', $path);
                if($i == 1)
                    $n->archivo1 = $savePath;
                elseif($i == 2)
                    $n->archivo2 = $savePath;
                else
                    $n->archivo3 = $savePath;
                $n->save();
            }
        }   
        dd($request->all());
        foreach ($etiquetas as $etiqueta) {
           if($request->input('etiqueta' . $etiqueta->id)){
               $n->etiquetaAutorNoticia()->attach($request->input('etiqueta' . $etiqueta->id), ['user_id' => $user]);
           }
        }
        $request->session()->flash('status', "Se guardo correctamente $n->titulo");

        /* NOTIFICACION DE UNA NUEVA NOTICIA DISPONIBLE */
        /*
        $users = User::all();
        foreach ($users as $user) {
           $user->notify(new NoticiaNueva());
           $user->notify(new NoticiaLeer($user, $n));
        }*/
        return redirect()->action([NoticiaController::class, 'index']);
        
    }

    public function show($id)
    {
       $noticia = Noticia::findOrFail($id);
       $users = User::pluck('name', 'id'); 
       $carreras = Carrera::pluck('carrera', 'id');
       $categorias = Categoria::pluck('categoria', 'id');
       return view('Noticia.show', compact('noticia', 'users', 'carreras', 'categorias'));
    }

    public function edit($id)
    {
        $noticia = Noticia::findOrFail($id);
        $users = User::pluck('name', 'id'); 
        $etiquetas = Etiqueta::pluck('nombre', 'id');
        $carreras = Carrera::pluck('carrera', 'id');
        $categorias = Categoria::pluck('categoria', 'id');
        // $archivo = Archivo::pluck('archivo', 'id');
        //dd($archivo);
        return view('Noticia.edit', compact('noticia', 'users', 'etiquetas', 'carreras', 'categorias'));
    }

    public function update(Request $request, $id)
    {
        $n = Noticia::findOrFail($id);
        $data = $request->validate([
            'titulo' => 'required|unique:noticias,titulo,' . $id,
            'cuerpo' => 'required',
            'autor' => 'required',
            'carrera_id' => 'required',
            // 'categoria_id' => 'required',
            'imagen' => 'image|max:2048'
        ]);

        if($request->hasFile('imagen')){
            $fileImg = $request->file('imagen');
            $path = $fileImg->storeAs('public/noticias/' . $n->id, $fileImg->getClientOriginalName()); 
            $savePath = str_replace('public/', '', $path);
            $n->imagen = $savePath;
        }

        for ($i=1; $i <=3; $i++) { 
            if($request->hasFile('archivo'.$i)){
                $file = $request->file('archivo'.$i);
                $path = $file->storeAs('public/archivos/' . $n->id, $file->getClientOriginalName()); 
                $savePath = str_replace('public/', '', $path);
                if($i == 1)
                    $n->archivo1 = $savePath;
                elseif($i == 2)
                    $n->archivo2 = $savePath;
                else
                    $n->archivo3 = $savePath;
                $n->save();
            }
        }

        $n->update($data); 
        $n->autor = $request->input('autor');
        $n->carrera_id = $request->input('carrera_id');
        $n->categoria_id = $request->input('categoria_id');

        $etiquetas = Etiqueta::all(); 
        $user = User::all()->random()->id;

        $n->save();
        //dd($n);
        //dd($request->all());
        $n->etiquetaAutorNoticia()->detach();

        foreach ($etiquetas as $etiqueta) {
           if($request->input('etiqueta' . $etiqueta->id)){
               $n->etiquetaAutorNoticia()->attach($request->input('etiqueta' . $etiqueta->id), ['user_id' => $user]);
           }
        }


        $request->session()->flash('status', "Se actualizó correctamente $n->titulo");
        return redirect()->action([NoticiaController::class, 'index']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $n = Noticia::findOrFail($id);
        $n->etiquetaAutorNoticia()->detach();
        $n->delete();

        // dd($id);
        Session::flash('status', 'Noticia eliminada con éxito');
        return redirect()->action([NoticiaController::class, 'index']);
    }
}
