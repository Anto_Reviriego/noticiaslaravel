<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class NoticiaEtiqueta extends Pivot
{
    use HasFactory;

    public function User(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
