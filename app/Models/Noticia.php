<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Noticia extends Model
{
    use HasFactory;
    use SoftDeletes; // borrado logico
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'titulo', 
        'cuerpo',
        'autor', 
        'archivo1', 
        'archivo2',
        'archivo3'
    ];
    
    public static function conImagen(){
        return Noticia::whereNotNull('imagen');
    }

    public function autorNoticia(){
        return $this->belongsTo(User::class , 'autor');
    }

    public function etiquetaAutorNoticia(){
        return $this->belongsToMany(Etiqueta::class, 'noticias_etiquetas')
                    ->withPivot('user_id')
                    ->withTimestamps()
                    ->using(NoticiaEtiqueta::class);
    }

    public function deCarrera(){
        return $this->belongsTo(Carrera::class, 'carrera_id');
    }

    public function categoria()
    {
       return $this->belongsTo(Categoria::class, 'categoria_id');
    }
}
