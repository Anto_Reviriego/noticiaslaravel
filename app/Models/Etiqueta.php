<?php

namespace App\Models;

use App\Models\Noticia;
use App\Models\NoticiaEtiqueta;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Etiqueta extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre', 
        'descripcion'
    ];
    
    public function noticiasEtiqueta(){
        return $this->belongsToMany(Noticia::class, 'noticias_etiquetas')
                    ->withPivot('user_id')
                    ->withTimestamps()
                    ->using(NoticiaEtiqueta::class);
    }
}
