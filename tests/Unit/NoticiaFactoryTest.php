<?php

namespace Tests\Unit;

use App\Models\Noticia;
use Tests\TestCase;

class NoticiaFactoryTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testNoticiaFactory()
    {
        $noticias = Noticia::factory()->count(1)->make();
        $noticia = $noticias->first();
        $this->assertInstanceOf(Noticia::class, $noticia); 
        $this->assertNotEmpty($noticia->titulo);
        $this->assertLessThanOrEqual(255, strlen($noticia->titulo));
        $this->assertNotEmpty($noticia->cuerpo); 
        $this->assertNotEmpty($noticia->autor);
    }
}
