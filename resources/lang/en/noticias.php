<?php

return array(
    /* NAV BAR */
    'titulo'        => 'News',
    'home'          => 'Home', 
    'noticia'       => 'News', 
    'withImg'       => 'With Images', 
    'create'        => 'Create News', 
    'idioma'        => 'Languages',
    'msjIdioma'     => 'The language was changed to :locale', 
    'espaniol'      => 'Spanish', 
    'ingles'        => 'English',
    'login'         => 'Login', 
    'logout'        => 'Logout',
    'registrar'     => 'Register',
    /* ABM */
    'btnVer'        => 'Details', 
    'btnEdit'       => 'Edit', 
    'btnDelete'     => 'Delete',
    /* CRUD */
    'tituloCrud'    => 'Title', 
    'cuerpoCrud'    => 'Body', 
    'imgCrud'       => 'Image', 
    'autorCrud'     => 'Author', 
    'carreraCrud'   => 'Career', 
    'categoriaCrud' => 'Category',
    'etiquetaCrud'  => 'Labels',
    'tituloPlaceholder'     => 'Enter a title', 
    'cuerpoPlaceholder'     => 'Enter a body for the news',
    'autorPlaceholder'      => 'Select the author of the news',
    'carreraPlaceholder'    => 'Select a career',
    'categoriaPlaceholder'  => 'Select a category for the news',
    'btnForm' => 'Save data',
);