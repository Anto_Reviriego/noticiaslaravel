<?php

return array(
    /* NAV BAR */
    'titulo'        => 'Noticias',
    'home'          => 'Inicio', 
    'noticia'       => 'Noticias', 
    'usuario'       => 'Usuarios',
    'carrera'       => 'Carreras',
    'etiquetas'     => 'Etiquetas',
    'withImg'       => 'Con Imagenes', 
    'create'        => 'Nueva Noticia', 
    'idioma'        => 'Idiomas',
    'msjIdioma'     => 'El idioma fue cambiado a :locale', 
    'espaniol'      => 'Español', 
    'ingles'        => 'Inglés',
    'login'         => 'Iniciar sesión', 
    'logout'        => 'Cerrar sesión',
    'registrar'     => 'Registarse',
    /* ABM */
    'btnVer'        => 'Ver detalles', 
    'btnEdit'       => 'Editar', 
    'btnDelete'     => 'Eliminar',
    /* CRUD */
    'tituloCrud'    => 'Titulo', 
    'cuerpoCrud'    => 'Cuerpo', 
    'imgCrud'       => 'Imagen', 
    'noImgCrud'     => 'Imagen no disponible', 
    'autorCrud'     => 'Autor', 
    'carreraCrud'   => 'Carrera', 
    'categoriaCrud' => 'Categoria',
    'etiquetaCrud'  => 'Etiquetas',
    'tituloPlaceholder'     => 'Ingrese un título', 
    'cuerpoPlaceholder'     => 'Ingrese el cuerpo de la noticia',
    'autorPlaceholder'      => 'Seleccione el autor de la noticia',
    'carreraPlaceholder'    => 'Seleccione una carrera',
    'categoriaPlaceholder'  => 'Seleccione una categoria para la noticia',
    'btnForm' => 'Guardar datos',
);