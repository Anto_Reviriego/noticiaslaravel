<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>{{ (__('noticias.titulo')) }}</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <nav class="container-fluid navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand  text-info disabled" href="#"> Noticias </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <ul class="navbar-nav">
                {{-- <li><a class="nav-link active" href="{{ route('index')}}">{{ (__('noticias.home')) }}</a></li> --}}
                @guest
                    <li><a class="nav-link" href="{{ route('noticia.todasNoticias')}}"> Noticias </a></li>
                @endguest
                @auth
                    <li><a class="nav-link" href="{{ route('carrera.index')}}"> Carreras </a></li>
                    <li><a class="nav-link" href="{{ route('materia.index')}}"> Materias </a></li>
                    <li><a class="nav-link" href="{{ route('usuario.index')}}"> Alumnos  </a></li>
                    <li><a class="nav-link" href="{{ route('examen.index')}}"> Exámenes </a></li>
                    <li><a class="nav-link" href="{{ route('usuario.index')}}"> Usuarios </a></li> 
                    <div class="dropdown-divider"></div>
                @endguest
            </ul>
            <ul class="navbar-nav ml-auto mr-3">
                {{-- <div class="dropdown">
                    <a class="btn  bg-dark dropdown-toggle" role="button" id="dropdownIdioma" data-bs-toggle="dropdown" title="idiomas" href="#">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-translate text-white" viewBox="0 0 16 16">
                            <path d="M4.545 6.714 4.11 8H3l1.862-5h1.284L8 8H6.833l-.435-1.286H4.545zm1.634-.736L5.5 3.956h-.049l-.679 2.022H6.18z" />
                            <path d="M0 2a2 2 0 0 1 2-2h7a2 2 0 0 1 2 2v3h3a2 2 0 0 1 2 2v7a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2v-3H2a2 2 0 0 1-2-2V2zm2-1a1 1 0 0 0-1 1v7a1 1 0 0 0 1 1h7a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H2zm7.138 9.995c.193.301.402.583.63.846-.748.575-1.673 1.001-2.768 1.292.178.217.451.635.555.867 1.125-.359 2.08-.844 2.886-1.494.777.665 1.739 1.165 2.93 1.472.133-.254.414-.673.629-.89-1.125-.253-2.057-.694-2.82-1.284.681-.747 1.222-1.651 1.621-2.757H14V8h-3v1.047h.765c-.318.844-.74 1.546-1.272 2.13a6.066 6.066 0 0 1-.415-.492 1.988 1.988 0 0 1-.94.31z" />
                        </svg>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownIdioma">
                        <a class="dropdown-item" href="{{ route('noticia.idioma', ['locale' => 'es']) }}">{{(__('noticias.espaniol'))}}</a>
                        <a class="dropdown-item" href="{{ route('noticia.idioma', ['locale' => 'en']) }}">{{(__('noticias.ingles'))}}</a>
                    </div>
                </div> --}}
                <!-- Authentication Links -->
                @auth
                    <li><a class="nav-link" href="{{ route('noticia.todasNoticias')}}"> Noticias </a></li>
                    <li><a class="nav-link" href="{{ route('etiqueta.index')}}"> Etiquetas </a></li>
                    <li><a class="nav-link" href="{{ route('noticia.create')}}"> Nueva Noticias </a></li>
                @endguest
                @guest
                    @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}"> Iniciar sesión </a>
                        </li>
                    @endif

                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}"> Registarse </a>
                        </li>
                    @endif
                    @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                               {{(__('noticias.logout'))}}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </nav>
    <section>
        <div class="jumbotron">
            <div class="container">
                <main class="py-4">
                    @yield('content')
                </main>
            </div>
        </div>
    </section>

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    {{-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous">
    </script> --}}
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/964e75bf64.js" crossorigin="anonymous"></script>
</body>

</html>
