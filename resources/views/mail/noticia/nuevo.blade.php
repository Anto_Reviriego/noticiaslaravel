@component('mail::message')
# Blog de Noticias

@component('mail::panel')
{{ $noticia->titulo }}
<p> {{Str::limit($noticia->cuerpo, 150, '...');}} </p>
@endcomponent

@component('mail::button', ['url' => $url])
Continuar leyendo ...
@endcomponent

Saludos,<br>
{{ config('app.name') }}
@endcomponent
