@extends('Layout')

@section('content')
    <h1 class="text-center text-primary"> Usuario </h1>
    @include('Usuario.Partial._Formulario')
@endsection