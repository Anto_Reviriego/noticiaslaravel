@extends('Layout')

@section('content')
    <div class="containe pr-5 pl-5">
        <h1 class="text-center text-primary"> Etiqueta </h1>
        <div class="card">
            <div class="card-body">
             <h2> Detalles de la etiqueta </h2>
                <p> <span class="font-weight-bold"> Nombre:</span> {{ $etiqueta->nombre}}</p>
                <p> <span class="font-weight-bold"> Descripción:</span> {{ $etiqueta->descripcion}}</p>
            </div>
        </div>
    </div>
    <a href="{{route('etiqueta.index')}}" class="btn btn-view mt-5 mr1"><i class="fas fa-arrow-left"></i> Volver al listado de etiquetas</a>
@endsection