@if(isset($etiqueta))
    {{ Form::model($etiqueta, ['method' => 'put', 'route' => ['etiqueta.update', $etiqueta->id], 'novalidate', 'autocomplete' => 'off']) }}
@else
    {{ Form::open(['route' => 'etiqueta.store', 'novalidate', 'autocomplete' => 'off']) }}
@endif
@csrf
<div class="form-group @if($errors->has('etiqueta')) has-error has-feedback @endIf">
    {{ Form::label('nombre', 'Nombre de la etiqueta', ['class' => 'control-label']) }}
    {{ Form::text('nombre', old('nombre'), ['class' => 'form-control', 'placeholder' =>  'Ingrese el nombre de la etiqueta']) }}
    @error('nombre')
    <span class="invalid-feedback d-block" role="alert"><strong>{{$message}}</strong></span>
    @enderror
</div>
<div class="form-group @if($errors->has('descripcion')) has-error has-feedback @endIf">
    {{ Form::label('descripcion', 'Descripcón', ['class' => 'control-label']) }}
    {{ Form::text('descripcion', old('titulo'), ['class' => 'form-control', 'placeholder' =>  'Ingrese su descripción']) }}
    @error('descripcion')
    <span class="invalid-feedback d-block" role="alert"><strong>{{$message}}</strong></span>
    @enderror
</div>
<button type="submit" class="btn btn-primary btn-lg btn-block"> Guardar datos </button>
{{ Form::close() }}
