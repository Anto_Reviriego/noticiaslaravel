@extends('Layout')

@section('content')
    <div class="containe pr-5 pl-5">
        <h1 class="text-center text-primary"> Nueva Etiqueta </h1>
        {{-- PASAR AL INDEX --}}
        @if(Session::has('status'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{Session('status')}}
            <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
        @include('Etiqueta.EtiquetaPartial._Formulario')
    </div>
@endsection