@extends('Layout')

@section('content')
<h2 class="text-center pb-4"> Listado de etiquetas </h2>
<div class="container">
    <a class="btn btn-outline-info m-2" href="{{route('etiqueta.create')}}" role="button"><i class="fas fa-plus-circle"></i> Crear nueva etiqueta</a>
    @if(Session::has('status'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        {{Session('status')}}
        <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
</div>
<table class="table">
    <thead class="thead-dark">
        <tr>
            <th scope="col">Nombre</th>
            <th scope="col">Descripción</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($etiquetas as $etiqueta)
        <tr>
            <td scope="col">{{ $etiqueta->nombre }}</td>
            <td scope="col">{{ $etiqueta->descripcion }}</td>
            <td scope="col">
                <button type="button" class="btn btn-danger mr-1" dusk="eliminarEtiqueta{{$etiqueta->id}}" data-bs-toggle="modal" data-bs-target="#modaldelete{{$etiqueta->id}}"><i class="fas fa-trash-alt text-light"></i></button>
                <a href="{{route('etiqueta.edit', ['Etiquetum' => $etiqueta->id])}}" class="btn btn-dark mr-1"><i class="far fa-edit text-light"></i></a>
                <a href="{{route('etiqueta.show', ['Etiquetum' => $etiqueta->id])}}" class="btn btn-view mr1"><i class="fas fa-eye text-light"></i></a>
            </td>
        </tr>
         @include('Etiqueta.delete') 
        @endforeach
    </tbody>
</table>
<hr>
<div class="d-flex justify-content-center">
    {!! $etiquetas->links() !!}
</div>
@endsection
