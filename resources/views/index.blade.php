@extends('Layout')

@section('content')
    @foreach($noticias as $key => $noticia)
        @if ($loop->first)
            <h1 class="text-success"> Mostrando {{$loop->count}} noticias</h1>
        @endif
        <h2 class="text-info font-weight-bold"> {{$noticia->titulo}}</h2> 
        <p>{!! $noticia->cuerpo !!}</p>
        <hr>
    @endforeach
@endsection
