@extends('Layout')

@section('content')
@if(Session::has('status'))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    {{Session('status')}}
    <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
@forelse($noticias as $noticia)
@if ($loop->iteration % 2 != 0)
<div class="row p-3">
    @endif
    <div class="col-6">
        <div class="card">
            @if($noticia->imagen)
                @if(Str::startsWith($noticia->imagen, 'http'))
                    <img src="{{ $noticia->imagen }}" class="card-img-top" alt="..." width="400" height="250">
                @else
                    <img src="{{ asset('./storage/'. $noticia->imagen) }}" class="card-img-top" alt="..." width="400" height="250">
                @endif
                <hr>
            @else
            <h5 class="text-center text-muted"> No hay imagen disponible </h5>
            <hr>
            @endif

            <div class="card-body">
                <h3 class="card-title text-info">{{ $noticia->titulo }} </h3>
                <div class="row">
                    <div class="col-6">
                        <small class="text-muted"><span class="font-weight-bold">Autor:</span> {{ $noticia->autorNoticia->name }}</small>
                    </div>
                    <div class="col-6">
                        <small class="text-muted"><span class="font-weight-bold">Carrera:</span>
                            @if($noticia->deCarrera == "")
                                Sin asignar
                            @else
                                {{$noticia->deCarrera->carrera}}
                            @endif
                        </small>
                    </div>
                </div>
                <p class="card-text">{{ $noticia->cuerpo }} </p>
                    @if($noticia->archivo1 != null || $noticia->archivo2 != null || $noticia->archivo3 != null)
                        @if($noticia->archivo1 && $noticia->archivo2 && $noticia->archivo3)
                            <span class="badge badge-warning"> 
                                <a href="{{ asset('./storage/'. $noticia->archivo1) }}"> {{ basename($noticia->archivo1)}} </a>
                            </span>  
                            <span class="badge badge-warning"> 
                                <a href="{{ asset('./storage/'. $noticia->archivo2) }}"> {{ basename($noticia->archivo2)}} </a>
                            </span>  
                            <span class="badge badge-warning"> 
                                <a href="{{ asset('./storage/'. $noticia->archivo3) }}"> {{ basename($noticia->archivo3)}} </a>
                            </span>  
                        @elseif($noticia->archivo1 || $noticia->archivo2 || $noticia->archivo3)
                            @if($noticia->archivo1)
                            <span class="badge badge-warning"> 
                                <a href="{{ asset('./storage/'. $noticia->archivo1) }}"> {{ basename($noticia->archivo1)}} </a>
                            </span>
                            @endif  
                            @if($noticia->archivo2)  
                                <span class="badge badge-warning"> 
                                    <a href="{{ asset('./storage/'. $noticia->archivo2) }}" target="_blank"> {{ basename($noticia->archivo2)}} </a>
                                </span>  
                            @endif  
                            @if($noticia->archivo3)  
                            <span class="badge badge-warning"> 
                                <a href="{{ asset('./storage/'. $noticia->archivo3) }}"> {{ basename($noticia->archivo3)}} </a>
                            </span>  
                            @endif              
                        @endif 
                    @endif
                <div class="">
                @foreach($noticia->etiquetaAutorNoticia as $e)
                <a href="{{ route('noticia.etiqueta', ['etiqueta' => $e->id]) }}">
                    <span class="badge badge-info"> {{$e->nombre}} </span>
                </a>
                @endforeach
                </div>
                <p class="card-text text-right"><small class="text-muted"> {{ $noticia->created_at->toFormattedDateString() }} </small></p>
            </div>
            @auth
                <div class="card-footer">
                    <div class="row">
                        <a href="{{ route('noticia.show', ['Noticium' => $noticia->id]) }}" class="text-center btn" data-toggle="tooltip" data-placement="top" title="Ver"><i class="fas fa-eye text-primary"></i></a>
                        <a href="{{ route('noticia.edit', ['Noticium' => $noticia->id]) }}" class="text-center btn" data-toggle="tooltip" data-placement="top" title="Editar"><i class="far fa-edit text-info"></i></a>
                        <a href="{{ route('noticia.destroy', ['Noticium' => $noticia->id]) }}" class="text-center btn" data-toggle="tooltip" data-placement="top" title="Borrar"><i class="fas fa-trash-alt text-danger"></i></a>
                    </div>
                </div>
            @endguest
        </div>
    </div>
    @if ($loop->iteration % 2 != 1)
</div>
<hr>
@endif
@empty
<p class="text-capitalize"> No hay noticias </p>
@endforelse
</div>
<hr>
<div class="d-flex justify-content-center">
    {!! $noticias->links() !!}
</div>
@endsection