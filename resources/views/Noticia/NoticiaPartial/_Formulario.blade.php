@if(isset($noticia))
{{ Form::model($noticia, ['method' => 'put', 'route' => ['noticia.update', $noticia->id], 'files' => true, 'novalidate', 'autocomplete' => 'off']) }}
@else
{{ Form::open(['route' => 'noticia.store', 'files' => true, 'novalidate', 'autocomplete' => 'off']) }}
@endif
@csrf
<div class="form-group @if($errors->has('titulo')) has-error has-feedback @endIf">
    {{ Form::label('titulo', 'Titulo', ['class' => 'control-label']) }}
    {{ Form::text('titulo', old('titulo'), ['class' => 'form-control', 'placeholder' =>  'Ingrese el titulo de la noticia']) }}
    @error('titulo')
    <span class="invalid-feedback d-block" role="alert"><strong>{{$message}}</strong></span>
    @enderror
</div>
<div class="form-group @if($errors->has('cuerpo')) has-error has-feedback @endIf">
    {{ Form::label('cuerpo', 'Cuerpo', ['class' => 'control-label']) }}
    {{ Form::textarea('cuerpo', old('cuerpo'), ['class' => 'form-control', 'placeholder' => 'Ingrese el cuerpo de la noticia']) }}
    @error('cuerpo')
    <span class="invalid-feedback d-block" role="alert"><strong>{{$message}}</strong></span>
    @enderror
</div>
<div class="form-group @if($errors->has('imagen')) has-error has-feedback @endIf">
    {{ Form::label('imagen', 'Imagen', ['class' => '', 'for' => 'imagen']) }}
    {{ Form::file('imagen')}}
    @error('imagen')
    <span class="invalid-feedback d-block" role="alert"><strong>{{$message}}</strong></span>
    @enderror
</div>
@if(isset($noticia))
<div class="form-group @if($errors->has('autor')) has-error has-feedback @endIf">
    {{ Form::label('autor', 'Autor', ['class' => 'control-label']) }}
    {{ Form::select('autor', $users, null, ['class' => 'form-control']) }}
    @error('autor')
    <span class="invalid-feedback d-block" role="alert"><strong>{{$message}}</strong></span>
    @enderror
</div>
@else
<div class="form-group @if($errors->has('autor')) has-error has-feedback @endIf">
    {{ Form::label('autor', 'Autor', ['class' => 'control-label']) }}
    {{ Form::select('autor', $users, $Login, ['class' => 'form-control', 'disabled', 'readonly']) }}
    @error('autor')
    <span class="invalid-feedback d-block" role="alert"><strong>{{$message}}</strong></span>
    @enderror
</div>
@endif

<div class="form-group @if($errors->has('carrera')) has-error has-feedback @endIf">
    {{ Form::label('carrera_id', 'Carrera', ['class' => 'control-label']) }}
    {{ Form::select('carrera_id', $carreras, null, ['class' => 'form-control', 'placeholder' => 'Seleccione una carrera']) }}
    @error('carrera_id')
    <span class="invalid-feedback d-block" role="alert"><strong>{{$message}}</strong></span>
    @enderror
</div>

<div class="form-group @if($errors->has('archivo')) has-error has-feedback @endIf">
    @if(isset($noticia))
    <div class="custom-file col-12  mb-3">
        {{ Form::label('archivo1', 'Archivo', ['class' => '', 'for' => 'archivo']) }}
        {{ Form::file('archivo1',  ['class' => '', 'for' => 'archivo1', 'id' => 'archivo1'])}}
        <a href="{{ asset('./storage/'.$noticia->archivo1) }}"> {{ basename($noticia->archivo1)}} </a>
    </div>
    <div class="custom-file col-12  mb-3">
        {{ Form::label('archivo2', 'Archivo', ['class' => '', 'for' => 'archivo']) }}
        {{ Form::file('archivo2',  ['class' => '', 'for' => 'archivo1', 'id' => 'archivo1'])}}
        <a href="{{ asset('./storage/'.$noticia->archivo2) }}"> {{ basename($noticia->archivo2)}} </a>
    </div>
    <div class="custom-file col-12  mb-3">
        {{ Form::label('archivo3', 'Archivo', ['class' => '', 'for' => 'archivo']) }}
        {{ Form::file('archivo3',  ['class' => '', 'for' => 'archivo3', 'id' => 'archivo3'])}}
        <a href="{{ asset('./storage/'.$noticia->archivo3) }}"> {{ basename($noticia->archivo3)}} </a>
    </div>
    @else
    @for($i = 1; $i <= 3; $i++) {{-- GENERA CAMPOS DE ARCHIVOS 3 --}} <div class="custom-file col-12  mb-3">
        {{ Form::label('archivo'.$i, 'Archivo', ['class' => '', 'for' => 'archivo']) }}
        {{ Form::file('archivo'.$i,  ['class' => '', 'for' => 'archivo'.$i, 'id' => 'archivo' .$i])}}
        @error('archivo'.$i)
        <span class="invalid-feedback d-block" role="alert"><strong>{{$message}}</strong></span>
    @enderror
</div>
@endfor
@endif

<div class="form-group">
    <ul class="lista-check">
        @if(isset($noticia))
        @foreach($etiquetas as $id => $nombre)
        <li>
            @if($noticia->etiquetaAutorNoticia()->find($id))
            {{ Form::checkbox('etiqueta'. $id, $id, 'X', ['id' => $id]) }}
            {{ Form::label($id, $nombre, ['class' => '']) }}
            @else
            {{ Form::checkbox('etiqueta'. $id, $id, null, ['id' => $id]) }}
            {{ Form::label($id, $nombre, ['class' => '']) }}
            @endif
        </li>
        @endforeach
        @else
        @foreach($etiquetas as $id => $nombre)
        <li>
            {{ Form::checkbox('etiqueta'. $id, $id, null, ['id' => $id]) }}
            {{ Form::label($id, $nombre,  ['id' => $id]) }}
        </li>
        @endforeach
        @endif
    </ul>
</div>
<button type="submit" class="btn btn-primary btn-lg btn-block"> {{ (__('noticias.btnForm')) }} </button>
{{ Form::close() }}