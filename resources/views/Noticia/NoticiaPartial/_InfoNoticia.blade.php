<div class="col-6 offset-1">
    <div class="card" style="width: 50rem">
        @if($noticia->imagen)
        @if(Str::startsWith($noticia->imagen, 'http'))
        <img src="{{ $noticia->imagen }}" class="card-img-top" alt="...">
        @else
        <img src="{{ asset('./storage/'. $noticia->imagen) }}" class="card-img-top" width="100px" alt="...">
        @endif
        @else
        <h5 class="text-center text-muted"> No hay imagen disponible </h5>
        <hr>
        @endif

        <div class="card-body">
            <h3 class="card-title text-info">{{ $noticia->titulo }} </h3>
            <div class="row">
                <div class="col-6">
                    <small class="text-muted"><span class="font-weight-bold">Autor:</span> {{ $noticia->autorNoticia->name }}</small>
                </div>
                <div class="col-6">
                    <small class="text-muted"><span class="font-weight-bold">Carrera:</span>
                        @if($noticia->deCarrera == "")
                            Sin asignar
                        @else
                            {{$noticia->deCarrera->carrera}}
                        @endif
                    </small>
                </div>
            </div>
            {{-- <div class="row">
                <div class="col-12">
                    <small class="badge badge-info"><span class="font-weight-bold">Categoria:</span>
                        @if($noticia->categoria == "")
                            Sin asignar
                        @else
                            {{$noticia->categoria->categoria}}
                        @endif
                    </small>
                </div>
            </div> --}}
            <p class="card-text">{{ $noticia->cuerpo }} </p>
            @if($noticia->archivo1 != null || $noticia->archivo2 != null || $noticia->archivo3 != null)
                        @if($noticia->archivo1 && $noticia->archivo2 && $noticia->archivo3)
                            <span class="badge badge-warning"> 
                                <a href="{{ asset('./storage/'. $noticia->archivo1) }}"> {{ basename($noticia->archivo1)}} </a>
                            </span>  
                            <span class="badge badge-warning"> 
                                <a href="{{ asset('./storage/'. $noticia->archivo2) }}"> {{ basename($noticia->archivo2)}} </a>
                            </span>  
                            <span class="badge badge-warning"> 
                                <a href="{{ asset('./storage/'. $noticia->archivo3) }}"> {{ basename($noticia->archivo3)}} </a>
                            </span>  
                        @elseif($noticia->archivo1 || $noticia->archivo2 || $noticia->archivo3)
                            @if($noticia->archivo1)
                            <span class="badge badge-warning"> 
                                <a href="{{ asset('./storage/'. $noticia->archivo1) }}"> {{ basename($noticia->archivo1)}} </a>
                            </span>
                            @endif  
                            @if($noticia->archivo2)  
                                <span class="badge badge-warning"> 
                                    <a href="{{ asset('./storage/'. $noticia->archivo2) }}"> {{ basename($noticia->archivo2)}} </a>
                                </span>  
                            @endif  
                            @if($noticia->archivo3)  
                            <span class="badge badge-warning"> 
                                <a href="{{ asset('./storage/'. $noticia->archivo3) }}"> {{ basename($noticia->archivo3)}} </a>
                            </span>  
                            @endif              
                        @endif 
                    @endif
            <div class="">
                @foreach($noticia->etiquetaAutorNoticia as $e)
                    <a href="{{ route('noticia.etiqueta', ['etiqueta' => $e->id]) }}">  <span class="badge badge-info"> {{$e->nombre}} </span></a>
                @endforeach
        </div>
        <p class="card-text text-right"><small class="text-muted"> {{ $noticia->created_at->toFormattedDateString() }} </small></p>
        </div>
        <div class="card-footer">
            <div class="row text-center">
                <div class="col-6">
                    {{ Form::model($noticia, ['method' => 'delete', 'route' => ['noticia.destroy', $noticia->id]]) }}
                    <button type="submit" class="btn btn-danger"><i class="bi bi-trash"></i> Eliminar noticia </button>
                    {{ Form::close() }}
                </div>
                <div class="col-6">
                    <a href="{{ route('noticia.todasNoticias') }}" class="text-center btn btn-secondary"><i class="bi bi-arrow-90deg-left"></i> Volver</a>
                </div>
            </div>
        </div>
    </div>
</div>