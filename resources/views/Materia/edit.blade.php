@extends('Layout')

@section('content')
    <h1 class="text-center text-primary"> Materia </h1>
    @include('Materia.Partial._Formulario')
@endsection