<?php

use Illuminate\Support\Str;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ExamenController;
use App\Http\Controllers\CarreraController;
use App\Http\Controllers\MateriaController;
use App\Http\Controllers\NoticiaController;
use App\Http\Controllers\EtiquetaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', [NoticiaController::class, 'index'])->name('index')->middleware('lang');
/* FILTROS */
Route::get('/Noticia/index/{page?}', [NoticiaController::class, 'index'])->name('noticia.todasNoticias')->middleware('lang');
Route::get('/Noticia/index/autor/{autor}/{page?}', [NoticiaController::class, 'cardNoticiaPorAutor'])->name('noticia.autor')->middleware('lang');
Route::get('/Noticia/index/etiqueta/{etiqueta}/{page?}', [NoticiaController::class, 'cardNoticiaPorEtiqueta'])->name('noticia.etiqueta')->middleware('lang');
Route::get('/Noticia/index/carrera/{carrera}/{page?}', [NoticiaController::class, 'cardNoticiaPorCarrera'])->name('noticia.carrera')->middleware('lang');
Route::middleware(['auth', 'lang'])->group(function (){
    /* RESOURCES */
    Route::resource('/Noticia', NoticiaController::class)->names([
        'create' => 'noticia.create',
        'store' => 'noticia.store',
        'edit' => 'noticia.edit', 
        'update' => 'noticia.update', 
        'show' => 'noticia.show',
        'destroy' => 'noticia.destroy'
    ]);

    /* ETIQUETA */
    Route::get('/Etiqueta/index', [EtiquetaController::class, 'index'])->name('etiqueta.index')->middleware('lang');

    /* RESOURCES */
    Route::resource('/Etiqueta', EtiquetaController::class)->names([
        'create' => 'etiqueta.create',
        'store' => 'etiqueta.store',
        'edit' => 'etiqueta.edit', 
        'update' => 'etiqueta.update', 
        'show' => 'etiqueta.show',
        'destroy' => 'etiqueta.destroy'
    ]);

    /* CARRERA */
    Route::get('/Carrera/index', [CarreraController::class, 'index'])->name('carrera.index')->middleware('lang');

    /* RESOURCES */
    Route::resource('/Carrera', CarreraController::class)->names([
        'create' => 'carrera.create',
        'store' => 'carrera.store',
        'edit' => 'carrera.edit', 
        'update' => 'carrera.update', 
        'show' => 'carrera.show',
        'destroy' => 'carrera.destroy'
    ]);

    /* USUARIO */
    Route::get('/Usuario/index', [UserController::class, 'index'])->name('usuario.index')->middleware('lang');

    /* RESOURCES */
    Route::resource('/Usuario', UserController::class)->names([
        'create' => 'usuario.create',
        'store' => 'usuario.store',
        'edit' => 'usuario.edit', 
        'update' => 'usuario.update', 
        'show' => 'usuario.show',
        'destroy' => 'usuario.destroy'
    ]);

    /* MATERIA */
    Route::get('/Materia/index', [MateriaController::class, 'index'])->name('materia.index')->middleware('lang');

    /* RESOURCES */
    Route::resource('/Materia', MateriaController::class)->names([
        'create' => 'materia.create',
        'store' => 'materia.store',
        'edit' => 'materia.edit', 
        'update' => 'materia.update', 
        'show' => 'materia.show',
        'destroy' => 'materia.destroy'
    ]);

    /* EXAMEN */
    Route::get('/Examen/index', [ExamenController::class, 'index'])->name('examen.index')->middleware('lang');

    /* RESOURCES */
    Route::resource('/Examen', ExamenController::class)->names([
        'create' => 'examen.create',
        'store' => 'examen.store',
        'edit' => 'examen.edit', 
        'update' => 'examen.update', 
        'show' => 'examen.show',
        'destroy' => 'examen.destroy'
    ]);
});



/* IDIOMAS */
Route::get('/Noticia/idioma/{locale}', function($locale){
    App::setLocale($locale); 
    session()->put('locale', $locale);
    $locale = Str::upper($locale); 
    Session()->flash('status', __('noticias.msjIdioma', ['locale' => $locale]));
    return redirect()->back();
})->name('noticia.idioma')->middleware('lang');